<?php

namespace tests\unit;

class TimeIntervalTest extends \Codeception\Test\Unit
{

    private static array $list = array (
        '09:00-11:00',
        '15:00-16:00',
        '17:00-20:00',
        '20:30-21:30',
        '21:30-22:30',
    );

    private static array $list_to_add = array (
      	'11:00-13:00',
      	'14:00-16:00',
        '09:00-11:00', //Wont be added
    );

    public function isTimeIntervalValid($timeInterval): bool
    {

        if (empty($timeInterval))
            return false;

        if(!preg_match("/^\d{2}:\d{2}-\d{2}:\d{2}$/", $timeInterval))
            return false;

        [$from, $to] = explode('-', $timeInterval);

        $fromDateTime = date_create_from_format('H:i', $from);
        $toDateTime = date_create_from_format('H:i', $to);

        if ($fromDateTime > $toDateTime)
            return false;

        return true;

    }

    public function canAdd($timeInterval): bool
    {
        foreach (self::$list as $interval){

            [$from, $to] = explode('-', $timeInterval);

            $fromDateTime = date_create_from_format('H:i', $from);
            $toDateTime = date_create_from_format('H:i', $to);

            [$from, $to] = explode('-', $interval);

            $_fromDateTime = date_create_from_format('H:i', $from);
            $_toDateTime = date_create_from_format('H:i', $to);

            if(!(
                ($fromDateTime < $_fromDateTime && $toDateTime <= $_toDateTime)
                    ||
                ($fromDateTime >= $_toDateTime && $toDateTime > $_toDateTime)
            ))
                return false;

        }

        return true;

    }

    public function testData(): void
    {

        foreach (self::$list as $interval){
            $this->assertTrue($this->isTimeIntervalValid($interval), "\033[1;31m\033[43mThe Interval '{$interval}' is not valid!\033[0m\033[0m");
        }

        foreach (self::$list_to_add as $interval){
            $this->assertTrue($this->canAdd($interval), "\033[1;31m\033[43mThe Interval '{$interval}' has crosshair with existing elements!\033[0m\033[0m");
        }

    }
}
