<?php

use yii\db\Migration;

/**
 * Class m220426_094847_create_orders
 */
class m220426_094847_create_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->unsigned(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP')->append(' ON UPDATE CURRENT_TIMESTAMP'),
        ]);
        $this->addCommentOnTable('orders', 'Заказы');
        //Not working with user_id UNSIGNED has to be INTEGER(11)
        //$this->addForeignKey('order-user_id', 'orders', 'user_id', 'users', 'id');
        $this->createIndex('order-user_id', 'orders', 'user_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('orders');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220426_094847_create_orders cannot be reverted.\n";

        return false;
    }
    */
}
