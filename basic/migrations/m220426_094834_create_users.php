<?php

use yii\db\Migration;

/**
 * Class m220426_094834_create_users
 */
class m220426_094834_create_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Имя покупателя'),
            'birthday_at' => $this->date()->comment('Дата рождения'),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP')->append(' ON UPDATE CURRENT_TIMESTAMP'),
        ]);
        $this->addCommentOnTable('users', 'Покупатели');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220426_094834_create_users cannot be reverted.\n";

        return false;
    }
    */
}
