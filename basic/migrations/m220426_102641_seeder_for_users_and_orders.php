<?php

use yii\db\Migration;

/**
 * Class m220426_102641_seeder_for_users_and_orders
 */
class m220426_102641_seeder_for_users_and_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('users', ['name' => 'alex73', 'birthday_at' => '1982-10-11']);
        $this->insert('users', ['name' => 'admin', 'birthday_at' => '1990-01-01']);
        $this->insert('users', ['name' => 'third client', 'birthday_at' => '1990-01-01']);

        $this->insert('orders', ['user_id' => 1]);
        $this->insert('orders', ['user_id' => 1]);
        $this->insert('orders', ['user_id' => 2]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220426_102641_seeder_for_users_and_orders cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220426_102641_seeder_for_users_and_orders cannot be reverted.\n";

        return false;
    }
    */
}
